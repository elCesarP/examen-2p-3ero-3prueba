﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Prueba_3_Poo
{
    class Sistemadesonido: iComponente
    {
        //Variable de referencia hacia quien decoramos
        private iComponente decoramosA;

        //Pasamos al objeto que va a ser decorado
        public Sistemadesonido(iComponente pComponente)
        {
            decoramosA = pComponente;
        }
        public override string ToString()
        {
            return "Radio M1000pro+\r\n" + decoramosA.ToString();
                       
        }
        //metodos de la interfaz
        public double Costo()
        {
            //calculamos el costo
            //el costo de lo que decoró mas mi costo como componente
            return decoramosA.Costo() + 3500;
        }  
        public string Funciona()
        {         
            return decoramosA.Funciona() + "Enciendo el radio";
                

        }
        
    }
    
}

