﻿using System;

namespace Examen_Prueba_3_Poo
{
    class Program
    {
        static void Main(string[] args)

        {
            //Creamos el componente centrar con tipo IComponente
            //para permitir la decoracion de un auto
            iComponente miAuto = new Auto("2018", "2 puertas", 200000);

            Console.WriteLine(miAuto);

            //Si necesitamos usar un metodo propio de Auto
            //necesitamos hacer uso de un type cast
            ((Auto)miAuto).Puertas(true);

            Console.WriteLine("----------");

            //DECORAMOS EL AUTO CON SISTEMA DE SONIDO
            miAuto = new Sistemadesonido(miAuto);

            //Comprobamos la adicion de las caracteristicas
            Console.WriteLine(miAuto.Costo());
            Console.WriteLine(miAuto.Funciona());
            Console.WriteLine(miAuto);

            Console.WriteLine("-----------");

            miAuto = new CNitrogeno(miAuto);

            Console.WriteLine(miAuto.Costo());
            Console.WriteLine(miAuto.Funciona());
            Console.WriteLine(miAuto);

            //Para usar algo propio del nitrogeno necesitamos UN TYPE CAST
            ((CNitrogeno)miAuto).UsanN();

            Console.WriteLine("--------");

            miAuto = new Suspension(miAuto);

            Console.WriteLine(miAuto.Costo());
            Console.WriteLine(miAuto.Funciona());
            Console.WriteLine(miAuto);

            Console.WriteLine("---------");

        }
    }
}
