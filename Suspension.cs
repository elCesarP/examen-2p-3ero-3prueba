﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Prueba_3_Poo
{
    class Suspension: iComponente
    {
        //Variable de referencia hacia quien decoramos
        private iComponente decoramosA;
        //Pasamos al objeto que va a ser decorado
        public Suspension(iComponente pComponente)
        {
            decoramosA = pComponente;
        }
        public override string ToString()
        {
            return "suspension de alto desempeño \r\n" + decoramosA.ToString();
        }
        //metodos de la interfaz
        public double Costo()
        {
            //calculamos el costo 
            //El costo de los que decoró mas mi costo como componente
            return decoramosA.Costo() + 6300;
        }
        public string Funciona()
        {
            return decoramosA.Funciona() + ",Elevando Suspension";
        }
    }
}
