﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Prueba_3_Poo
{
    class Auto : iComponente
    {
        private string modelo;
        private string caracteristicas;
        public double costo;

        public Auto(string pModelo, string pCaract, double pCosto)
        {
            modelo = pModelo;
            caracteristicas = pCaract;
            costo = pCosto;

        }
        public void Puertas (bool pEstado)
        {
            if (pEstado)
                Console.WriteLine("Puertas Cerradas");
            else
                Console.WriteLine("Puertas Abiertas");
        }
        public override string ToString()
        {
            return string.Format("Modelo {0}, {1} \r\n", modelo, caracteristicas);
        }
        //Estos son los metodos implementados iCompomente
        public double Costo()
        {
            return costo;

        }
        public string Funciona()
        {
            return "Encendi el motor";
        }
    }
}
