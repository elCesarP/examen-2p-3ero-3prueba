﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Prueba_3_Poo
{
    class CNitrogeno :iComponente
    {
        //variable de referencia hacia quien decoramos
        private iComponente decoramosA;

        //Pasamos al objeto que ca a ser decorado
        public CNitrogeno(iComponente pComponente)
        {
            decoramosA = pComponente;
        }

        public override string ToString()
        {
            return "SISTEMA DE NITROGENO\r\n" + decoramosA.ToString();

        }
        //Metodo de la interfaz
        public double Costo()
        {
            //Calculamos el costo 
            //El costo de lo que decoro mas mi costo como componente 
            return decoramosA.Costo() + 45000;
        }
        public string Funciona()
        {
            return decoramosA.Funciona() + "Nitrogeno listo";
        }
        public void UsanN()
        {
            Console.WriteLine("Nitrogeno en uso");
        }
    }
}
